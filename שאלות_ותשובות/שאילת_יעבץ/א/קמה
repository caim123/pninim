% שאלה קמה

ג *ומה* ששאלת בענין אמירת כל נדרי אם י"ל דאינדרנא ונפשאי לשון יחיד כמ"ש הלבוש. וגם הט"ז השוה עמו במלת אינדרנא. אע"ג דפליג במלות נפשאי נדראי כו'. כמאן נעביד:

*תשובה*

[^1]*כבר* כתבתי בחיבורי שכל מה שטרחו אלו הרבנים ושאר האחרונים ז"ל לפרש נוסח כל נדרי ע"ד ר"ת הכל טעות ושיבוש בלשון במ"כ. ולא אחד בהם שידע והכיר לשון ארמי על בוריו. כי האמת הברור שהנוסח הקדמון הוראתו על הנדרים שעברו. דינדרנא כו' הוא ודאי עבר. אבל אין ספק בעולם שע"פ דעת ר"ת צריכין אנו על כרחנו לשנות הנוסח הישן לגמרי וצ"ל דננדר או דנדר נו"ן חרוקה. והדל"ת בדג"ש מקום החסרה. וכן כולם דנשתבע ודניסר על נפשתנא. הכל בנו"ן האית"ן דרך אחד הוא. ללשון תרגום עם ל"הק בזה בבירור. ולכן לא יפה אנחנו עושים פה היום לאחוז החבל בשני ראשיו ולזכות שטרי {לוח תיקונים: שטרא} לבי תרי. להחזיק בנוסח הישן במקצתו ולעזוב מקצתו ולשנותו ולהפכו להבא. שזה אי אפשר כלל ואינו סובל השינוי והתמורה. ואין מקום לקיימו עפ"ד ר"ת. אם לא נחדשהו מעיקרו ונהפכהו כולו מעבר לעתיד. אבל נראין דברי הקדמונים ז"ל שנתקן על העבר. ואין לי בכך ספק בעולם. מ"מ לחוש לדר"ת הואיל ויצא מפי אותו צדיק. אני נוהג לומר בשתי הלשונות ולכפול הנוסח דנדרנא ודנידר דאשתבענא ודנשתבע דאסרנא ודניסר כו' מיום כפורים שעבר כו' ומי"כ זה עד כו' לצאת ידי הכל. עם שבלא"ה אנו מתירין נדרים שעברו ומתנין על שלהבא בכנופיא ביי"ת. מ"מ מנהג שהוקבע הוקבע:

[^1] נוסח כל נדרי שלנו מורכב אתרי ריכשי ועלתה בו תמור"ה זרה ותיקונו הנכון
